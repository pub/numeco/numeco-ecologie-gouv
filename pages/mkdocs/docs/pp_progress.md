# Évolution de l'impact de l'onglet Politiques Publiques de ecologie.gouv.fr dans le temps

## Nombre de requêtes

```vegalite
{
  "$schema": "https://vega.github.io/schema/vega-lite/v5.json",
  "description": "Nombre de requêtes",
  "use_data_path": false,
  "data": {"url": "/data/politiques_publiques_time.csv"},
  "transform": [{"filter": "datum.stat==='REQUESTS'"}],
  "mark": {
    "type": "line",
    "point": {
      "filled": false,
      "fill": "white"
    }
  },
  "encoding": {
    "x": {"field": "date", "type": "temporal", "title": "Date d'analyse", "axis": {"format": "%d/%m/%y"}},
    "y": {"field": "value", "type": "quantitative", "title": "Nombre de requêtes"},
    "color": {"field": "type", "type": "nominal", "title" : "Type"}
  }
}
```

## Données transférées en Mo

```vegalite
{
  "$schema": "https://vega.github.io/schema/vega-lite/v5.json",
  "description": "Données transférées en Mo",
    "use_data_path": false,
  "data": {"url": "/data/politiques_publiques_time.csv"},
  "transform": [{"filter": "datum.stat==='TRANSFERED'"},{"calculate": "datum.value/1000000", "as": "mo"}],
  "mark": {
    "type": "line",
    "point": {
      "filled": false,
      "fill": "white"
    }
  },
  "encoding": {
    "x": {"field": "date", "type": "temporal", "title": "Date d'analyse", "axis": {"format": "%d/%m/%y"}},
    "y": {"field": "mo", "type": "quantitative", "title": "Données transférées (Mo)"},
    "color": {"field": "type", "type": "nominal", "title" : "Type"}
  }
}
```

## Nombre d'éléments du DOM

```vegalite
{
  "$schema": "https://vega.github.io/schema/vega-lite/v5.json",
  "description": "Nombre d'éléments du DOM",
  "data": {"url": "/data/politiques_publiques_time.csv"},
  "transform": [{"filter": "datum.stat==='ELEMENTS'"}],
  "mark": {
    "type": "line",
    "point": {
      "filled": false,
      "fill": "white"
    }
  },
  "encoding": {
    "x": {"field": "date", "type": "temporal", "title": "Date d'analyse", "axis": {"format": "%d/%m/%y"}},
    "y": {"field": "value", "type": "quantitative", "title": "Nombre d'éléments"},
    "color": {"field": "type", "type": "nominal", "title" : "Type"}
  }
}
```

## Nombre d'images de plus de 100 ko
```vegalite
{
  "$schema": "https://vega.github.io/schema/vega-lite/v5.json",
  "description": "Nombre d'images de plus de 100 ko",
  "data": {"url": "/data/pp_images_time.csv"},
  "mark": {
    "type": "line",
    "point": {
      "filled": false,
      "fill": "white"
    }
  },
  "encoding": {
    "x": {"field": "date", "type": "temporal", "title": "Date d'analyse", "axis": {"format": "%d/%m/%y"}},
    "y": {"field": "value", "type": "quantitative", "title": "Nombre d'images"}
  }
}
```