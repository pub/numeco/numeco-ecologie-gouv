import {convertJsonToList, newAnalysisPolPubliques} from './analysis.js';
import fs from 'fs';

const NUM = process.env.NUM || 0;
const target = '../target';

/**
 * Get polp files
 */
function getPolPFiles() {
  let polPFiles = [];
  const regex = /polp\d+.json/;

  fs.readdirSync(target)
    .map(file => {
      if (file.match(regex)) polPFiles.push(file);
    })
  
  return polPFiles;
}

if (process.env.DEBUG) {
  newAnalysisPolPubliques(3);
} else {
  if (NUM === 0) {
    const allPolPFiles = getPolPFiles();
    console.log(`Veuillez indiquer le numéro du fichier à analyser (${allPolPFiles.length} fichiers disponibles)`);
    process.exit(1);
  } else {
    newAnalysisPolPubliques(await convertJsonToList(`../target/polp${NUM}.json`), NUM);
  }
}
