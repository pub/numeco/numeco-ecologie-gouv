import { JSDOM } from 'jsdom';
import libxmljs, { parseXmlAsync } from 'libxmljs';
import * as fs from 'node:fs';

const BASE_URL = process.env.BASE_URL ? process.env.BASE_URL : 'https://www.ecologie.gouv.fr';
//const fs = require('fs')


export async function getActuFile(actus) {
  const path_file = '../target/actu.json';
  fs.writeFile(path_file, JSON.stringify(actus), (err) => {
    if (err) throw err;
  })
  return path_file;
}
/**
 * Get Actualités links
 * @param {*} nbActu
 */
export async function getActu(nbActu) {
  let index = 0;
  const urls = [];
  while (true) {
    if (nbActu - urls.length <= 0) {
      break;
    }
    const newUrls = await getArticles({
      xpathNode: '//div[@class="fr-card__content"]',
      xpathUrl: 'string(descendant::h3/a/@href)',
      xpathTitle: 'string(descendant::h3/a/span/text())',
      xpathDate:
        'string(descendant::div[@class="fr-card__end"]/p/text())',
      url: `${BASE_URL}/actualites?keys=&type[news]=news&page=${index}`,
      nb: nbActu - urls.length,
    });
    if (newUrls.length === 0) {
      break;
    }
    urls.push(
      ...newUrls.slice(0, Math.min(nbActu - urls.length, newUrls.length)));
    index++;
    await new Promise((resolve) => setTimeout(resolve, 200));
  }
  return urls;
}

export async function getRdvFile(rdvs) {
  const path_file = '../target/rdv.json'
  fs.writeFile(path_file, JSON.stringify(rdvs), (err) => {
    if (err) throw err;
  })
  return path_file;
}

/**
 * Get Rendez-vous links
 * @param {*} nbLinks
 */
export async function getRendezvous(nbLinks) {
  let index = 0;
  const urls = [];
  while (true) {
    if (nbLinks - urls.length <= 0) {
      break;
    }
    const newUrls = await getArticles({
      xpathNode: '//div[@class="fr-card__content"]',
      xpathUrl: 'string(descendant::h3/a/@href)',
      xpathTitle: 'string(descendant::h3/a/span/text())',
      xpathDate:
        'string(descendant::div[@class="fr-card__end"]/p/text())',
      url: `${BASE_URL}/actualites?keys=&type[appointment]=appointment&page=${index}`,
      nb: nbLinks - urls.length,

    });
    if (newUrls.length === 0) {
      break;
    }
    urls.push(
      ...newUrls.slice(0, Math.min(nbLinks - urls.length, newUrls.length)));
    index++;
    await new Promise((resolve) => setTimeout(resolve, 200));
  }
  return urls;
}

export async function getPolPublicFile(pols) {
  let n = 1;
  const limit = 250;
  const uniquePols = [...new Map(pols.map(pol => [pol.url, pol])).values()]
  for (let i = 0; i < uniquePols.length; i += limit) {
    let pol = uniquePols.slice(i, (i + limit));
    fs.writeFile(`../target/polp${n}.json`, JSON.stringify(pol), (err) => {
      if (err) throw err;
    });
    n++;
  }
  return true;
}

export async function getThemes() {
  const cpt_timeout = 3;
  let cpt = 0;
  const urls = [];
  let url = `${BASE_URL}/nos-actions`;
  while (true) {
    try {
      console.error(`Fetching ${url} ...`);
      const response = await fetch(url);
      const data = await response.text();

      const dom = new JSDOM(data);
      const document = dom.window.document;

      const xpathResult = document.evaluate(
        "//a[@class='fr-tile__link' and contains(@href, '/politiques-publiques/')]/@href",
        document,
        null,
        dom.window.XPathResult.ORDERED_NODE_ITERATOR_TYPE,
        null,
      );

      let node = xpathResult.iterateNext();
      while (node) {
        urls.push(node.textContent);
        node = xpathResult.iterateNext();
      }
      return urls;
    } catch (error) {
      if (cpt < cpt_timeout) {
        cpt += 1;
        continue;
      }
      else {
        throw error;
        break;
      }
    }
  }
}

export async function getPolPublics(nb) {
  let themes_urls = await getThemes();
  console.log(themes_urls);
  const all_urls = [];
  for (const theme of themes_urls) {
    const newUrls = await getArticles({
      xpathNode:
        '//a[contains(@class,"fr-link") and contains(@href, "/politiques-publiques/")]',
      xpathTitle:
        'string(descendant::span/text())',
      xpathUrl: 'string(@href)',
      xpathDate:
        'string(descendant::span/text())',
      dateParser: (input) => 'Inconnue',
      url: `${BASE_URL}${theme}`,
      nb: nb,
    });
    all_urls.push(
      ...newUrls.slice(0, Math.min(nb - all_urls.length, newUrls.length)));
    await new Promise((resolve) => setTimeout(resolve, 200));
  }
  return remove_duplicates(all_urls);
}

function remove_duplicates(results) {
  // Remove duplicates from results, by comparing url field
  const uniqueResults = [...new Map(results.map(result => [result.url, result])).values()];
  return uniqueResults;
}
export async function getDossiersPresseFile(dossiers) {
  const path_file = '../target/dossiers_presse.json';
  fs.writeFile(path_file, JSON.stringify(dossiers), (err) => {
    if (err) throw err;
  })
  return path_file;
}

export async function getDossiersPresse(nb) {
  let index = 0;
  const urls = [];
  while (true) {
    if (nb - urls.length <= 0) {
      break;
    }
    const newUrls = await getArticles({
      xpathNode: '//div[@class="fr-card__content"]',
      xpathUrl: 'string(descendant::h3/a/@href)',
      xpathTitle: 'string(descendant::h3/a/span/text())',
      xpathDate:
        'string(descendant::div[@class="fr-card__end"]/p/text())',
      url: `${BASE_URL}/presse?keys=&op=Rechercher&field_press_type_tref[148]=148&page=${index}`,
      nb: nb - urls.length,
    });
    if (newUrls.length === 0) {
      break;
    }
    urls.push(
      ...newUrls.slice(0, Math.min(nb - urls.length, newUrls.length)));
    index++;
    await new Promise((resolve) => setTimeout(resolve, 200));
  }
  return urls;
}

/**
 * Generic function to get links
 * @param {*} param0
 */
async function getArticles({
  xpathNode,
  xpathUrl,
  xpathTitle,
  xpathDate,
  dateParser,
  url,
  nb,
}) {
  const cpt_timeout = 3;
  let cpt = 0;
  const urls = [];
  if (nb <= 0) {
    return urls;
  }
  while (true) {
    try {
      console.error(`Fetching ${url} ...`);
      const response = await fetch(url);
      const data = await response.text();

      const dom = new JSDOM(data);
      const document = dom.window.document;

      const xpathResult = document.evaluate(
        xpathNode,
        document,
        null,
        dom.window.XPathResult.ORDERED_NODE_ITERATOR_TYPE,
        null,
      );

      let node = xpathResult.iterateNext();
      while (node) {
        let date = document.evaluate(
          xpathDate,
          node,
          null,
          dom.window.XPathResult.STRING_TYPE,
          null,
        ).stringValue;
        if (dateParser) {
          date = dateParser(date);
        }
        urls.push({
          url: toAbsoluteUrl(
            document.evaluate(
              xpathUrl,
              node,
              null,
              dom.window.XPathResult.STRING_TYPE,
              null,
            ).stringValue,
          ),
          title: document.evaluate(
            xpathTitle,
            node,
            null,
            dom.window.XPathResult.STRING_TYPE,
            null,
          ).stringValue,
          date,
        });
        if (urls.length === nb) {
          return urls;
        }
        node = xpathResult.iterateNext();
      }
      return urls;
    }
    catch (error) {
      if (cpt < cpt_timeout) {
        cpt += 1;
        continue;
      }
      else {
        throw error;
        break;
      }
    }
  }
}

/**
 * Convert captured URL into absolute URL
 * @param {*} value
 * @return {*}
 */
function toAbsoluteUrl(value) {
  if (value.startsWith('http')) {
    return value;
  } else if (value.startsWith('/')) {
    return `${BASE_URL}${value}`;
  } else {
    return `${BASE_URL}/${value}`;
  }
}
